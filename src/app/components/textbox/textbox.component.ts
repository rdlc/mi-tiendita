import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.scss']
})
export class TextboxComponent implements OnInit {

  constructor() { }

  @Input() typeTextBox: string = "text";

  @Input() nameLabel: string = "";

  ngOnInit(): void {
  }

}
